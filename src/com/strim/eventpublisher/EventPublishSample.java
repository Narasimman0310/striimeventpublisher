package com.strim.eventpublisher;
import com.webaction.client.AuthContext;


import com.webaction.client.ClientFactory;
import com.webaction.client.WebActionClient;
import com.webaction.client.WebActionStream;
import com.webaction.client.auth.UsernamePasswordAuth;
import com.webaction.uuid.UUID;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;	
import org.joda.time.format.DateTimeFormatter;

import java.net.URI;
import java.util.HashMap;
import java.util.Random;

public class EventPublishSample {
    //Predefined order data to be published
	private static Logger logger = Logger.getLogger(EventPublishSample.class);
    private static String[][] data = {
            {"1074","GEORGETOWN","Washington","Distr. Of Columbia","20007","8221909019294180684","107402483905227379023997","IN20100/X","$149.00","20130801080012"},
            {"1226","WILLOW BEND","Plano","Texas","75093","5030910223430094829","122602483905227379023997","IC299901/S","$19.00","20130801080012"},
            {"1089","INTERNATIONAL PLAZA","Tampa","Florida","33607","3003317277875241575","108902483905227379023997","IC199901/A","$39.00","20130801080012"},
            {"1221","STONEBRIAR","Frisco","Texas","75034","2049630884731530518","122102483905227379023997","IP40000/Z","$450.00","20130801080012"},
            {"1176","UPPER WEST SIDE","New York","New York","10023","2907934702553981837","117602483905227379023997","IC299901/S","$19.00","20130801080012"},
            {"1005","ARROWHEAD","Glendale","Arizona","85308","8522583866581082278","100502483905227379023997","AT02340/A","$299.00","20130801080012"},
            {"1132","NORTHSHORE","Peabody","Massachusetts","01960","2856170679665245222","113202483905227379023997","AT210004/Z","$99.00","20130801080012"},
            {"1084","LINCOLN ROAD","Miami Beach","Florida","33139","3457283036831408837","108402483905227379023997","AE20004/A","$199.00","20130801080012"},
            {"1054","THIRD STREET PROMENADE","Santa Monica","California","90401","1920192740005900729","105402483905227379023997","IC199901/A","$19.00","20130801080012"},
            {"1011","4TH STREET","Berkeley","California","94710","2943640198337016807","101102483905227379023997","AE20004/A","$199.00","20130801080012"},
            {"1231","THE WOODLANDS","The Woodlands","Texas","77380","5848048622613091047","123102483905227379023997","IG139901/A","$23.00","20130801080012"},
            {"1101","NORTH MICHIGAN AVENUE","Chicago","Illinois","60611","9455883987905567267","110102483905227379023997","AT02340/A","$299.00","20130801080012"},
            {"1195","PIONEER PLACE","Portland","Oregon","97204","9916391118458814337","119502483905227379023997","IG139901/A","$23.00","20130801080012"},
            {"1219","UNIVERSITY PARK VILLAGE","Fort Worth","Texas","76107","1661166216674567378","121902483905227379023997","IA19201/S","$599.00","20130801080012"},
            {"1092","LENOX SQUARE","Atlanta","Georgia","30326","8668430224306840561","109202483905227379023997","AE20004/A","$199.00","20130801080012"},
            {"1027","CENTURY CITY","Los Angeles","California","90067","0553095077653905200","102702483905227379023997","IG139901/A","$23.00","20130801080012"},
            {"1019","SOUTH COAST PLAZA","Costa Mesa","California","92626","7583834928004661681","101902483905227379023997","IC199901/A","$19.00","20130801080012"},
            {"1188","CRABTREE VALLEY MALL","Raleigh","North Carolina","27612","2894459210434273139","118802483905227379023997","IM634033/R","$329.00","20130801080012"},
            {"1228","NORTH STAR","San Antonio","Texas","78216","0686039936003469517","122802483905227379023997","AT02340/A","$299.00","20130801080012"}
    };

    private static DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyyMMddHHmmss");

    public static void main (String[] args) throws Exception {
        //#1. Declare an instance of WebActionClient.
        // This is done outside the try-catch block to
        // allow us to call close in the finally block which
        // ensures all resources associated with the client are released
    	PropertyConfigurator.configure("conf/log4j.properties");
    	logger.setLevel(Level.INFO);
        WebActionClient client = null;

        try{
            //#2. Create an instance of URI pointing to the
            // WebAction server.
            // Please note the URI format consist of protocol being watp
            // and address of one of the nodes of WebAction Server with port being
            // http port(9080 by default)
            URI wauri = new URI("watp://10.10.112.233:9080");

            //#3. Create an intance of AuthContext.
            //Currently only UsernamePasswordAuthentication is supported
            // use that to provide username credentials of a webaction
            // user having sufficient privileges over the stream to which you
            // want to publish
            AuthContext authContext = new UsernamePasswordAuth("admin", "indium");

            //#4. Retrieve an instance of client from ClientFactory
            // by passing in uri and authcontext instances.
            // ClientFactory might return a cached client instances
            client = ClientFactory.getClientInstance(wauri, authContext);

            //#5. Retrieve an instance of stream from the client.
            // Stream name is passed as a Fully qualified stream name
            // use "list streams" command in console to get the stream name
            WebActionStream stream = client.getStream("admin:OrderStream");

            int count =0;

            //#6. Create an appropriate Event instance which is appropriate to the Stream to which
            // you want to publish.
            //In this example , the Stream OrderStream is of type OrderType.
            // The OrderType is exported as a java class into a jar and
            // hence is available to be used as an event to be populated and
            // published into the stream

            for (int i = 0; i < data.length; i++) {
                wa.admin.OrderType_1_0 evt = new wa.admin.OrderType_1_0(System.currentTimeMillis());

                evt.setStoreId(data[i][0]);
                evt.setCity(data[i][2]);
                evt.setState(data[i][3]);
                evt.setZip(data[i][4]);

                evt.setOrderId(data[i][6]);
                evt.setSku(data[i][7]);
                evt.setOrderAmount(Double.parseDouble(data[i][8].substring(1)));

                DateTime dt = formatter.parseDateTime(data[i][9]);
                evt.setDateTime(dt);
                evt.setHourValue(dt.hourOfDay().get());

                stream.publish(evt);
            }


            Thread.sleep(5000);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //#7. Close the client
            if (client != null)
                client.close();
        }

    }
}
